FROM adoptopenjdk/openjdk8:alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app-vagas.jar
CMD ["java", "-jar", "/app-vagas.jar"]